package ru.deathcry.helmes.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Builder
public class NewSectorDto {
    @NotNull
    @Size(min = 4)
    private String name;

    private Long parentId;
}
package ru.deathcry.helmes.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;
import ru.deathcry.helmes.entity.Sector;

import javax.persistence.OrderBy;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Data
@Builder
public class SectorDTO {
    private Long id;
    private String label;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OrderBy("label")
    private List<SectorDTO> children;

    public static final Function<Sector, SectorDTO> mapToSectorDTO =
            p -> SectorDTO.builder().id(p.getId()).label(p.getName()).children(p.getChildren()).build();

    public static class SectorDTOBuilder {
        public SectorDTOBuilder children(Set<Sector> children) {
            this.children = children.stream().map(mapToSectorDTO).collect(Collectors.toList());
            return this;
        }
    }
}


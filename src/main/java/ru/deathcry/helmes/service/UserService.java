package ru.deathcry.helmes.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.deathcry.helmes.dto.UserDto;
import ru.deathcry.helmes.entity.Sector;
import ru.deathcry.helmes.entity.User;
import ru.deathcry.helmes.repository.SectorRepository;
import ru.deathcry.helmes.repository.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SectorRepository sectorRepository;

    public List<?> getUsers() {
        return userRepository.findAll();
    }

    public Optional<User> getUser(long id) {
        return userRepository.findById(id);
    }

    public Optional<User> getUser(String name) {
        return userRepository.findByName(name);
    }

    public boolean userExist(String name) {
        return userRepository.existsByName(name);
    }

    public User addUser(UserDto dto) {
        if(!dto.isAgreedWithTerms()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You must agree with Terms!");
        }

        if(userRepository.findByName(dto.getName()).isPresent()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Such person is already in database!");
        }

        List<Sector> sectors = getSectorsFromIds(dto.getSectorIds());

        return userRepository.save(new User(dto.getName(), sectors, dto.isAgreedWithTerms()));
    }

    public User saveUser(UserDto dto){
        if(!userExist(dto.getName())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Such person does not exist!");
        }

        User tmp = userRepository.findByName(dto.getName()).get();

        List<Sector> sectors = getSectorsFromIds(dto.getSectorIds());

        tmp.setSectors(sectors);
        return userRepository.save(tmp);
    }

    private List<Sector> getSectorsFromIds(List<Integer> ids) {
        return ids.stream()
                .map(x -> sectorRepository.findById((long) x))
                .map(Optional::get)
                .collect(Collectors.toList());
    }
}
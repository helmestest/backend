package ru.deathcry.helmes.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.deathcry.helmes.dto.NewSectorDto;
import ru.deathcry.helmes.entity.Sector;
import ru.deathcry.helmes.repository.SectorRepository;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.deathcry.helmes.dto.SectorDTO.mapToSectorDTO;

@Service
public class SectorService {

    @Autowired
    private SectorRepository sectorsRepository;

    public List<?> getSectors(String mode) {
        if ("tree".equals(mode)) {
            return sectorsRepository.findAllRootSectors().stream().map(mapToSectorDTO).collect(Collectors.toList());
        }
        return sectorsRepository.findAll();
    }

    public Optional<?> getSector(String mode, long id) {
        Optional<Sector> sector = sectorsRepository.findById(id);
        if ("tree".equals(mode)) {
            return sector.map(mapToSectorDTO);
        }
        return sector;
    }

    public Optional<?> getSector(String mode, String name) {
        Optional<Sector> sector = sectorsRepository.findByName(name);
        if ("tree".equals(mode)) {
            return sector.map(mapToSectorDTO);
        }
        return sector;
    }

    public void addSector(NewSectorDto dto) {
        if (dto.getParentId() == null) {
            sectorsRepository.save(new Sector(dto.getName(), null));
            return;
        }

        Optional<Sector> tmp = sectorsRepository.findById(dto.getParentId());
        if (tmp.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Parent sector not found by provided ID");
        }

        sectorsRepository.save(new Sector(dto.getName(), tmp.get()));
    }

    @Value("classpath:data/defaultSectors.txt")
    Resource resourceFile;

    public void initBasicSectors() {
        sectorsRepository.deleteAll();
        List<String> lines;
        try {
            lines = new BufferedReader(new InputStreamReader(resourceFile.getInputStream(),
                            StandardCharsets.UTF_8)).lines().collect(Collectors.toList());
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Failed to read default sectors file");
        }
        Map<Integer, Sector> parents = new HashMap<>();
        for (String line : lines) {
            String name = line.trim();
            int tabs = line.length() - name.length();
            Sector entity = new Sector(name, parents.get(tabs));
            sectorsRepository.save(entity);
            parents.put(tabs + 1, entity);
        }
    }
}
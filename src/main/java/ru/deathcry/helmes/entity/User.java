package ru.deathcry.helmes.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "Users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true, length = 64, nullable = false)
    private String name;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @OrderBy("name")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Collection<Sector> sectors = new HashSet<>();

    @Column
    private boolean agreedWithTerms;

    public User() {
    }

    public User(String name, Collection<Sector> sectors, boolean agreedWithTerms) {
        this.name = name;
        this.sectors = sectors;
        this.agreedWithTerms = agreedWithTerms;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAgreedWithTerms() {
        return agreedWithTerms;
    }

    public void setAgreedWithTerms(boolean agreedWithTerms) {
        this.agreedWithTerms = agreedWithTerms;
    }

    public List<Long> getSectorIds() {
        return sectors.stream().map(Sector::getId).collect(Collectors.toList());
    }

    public Collection<Sector> getSectors() {
        return sectors;
    }

    public void setSectors(Collection<Sector> sectors) {
        this.sectors = sectors;
    }
}

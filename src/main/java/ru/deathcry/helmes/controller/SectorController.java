package ru.deathcry.helmes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.deathcry.helmes.dto.NewSectorDto;
import ru.deathcry.helmes.service.SectorService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/sectors")
public class SectorController {

    @Autowired
    private SectorService sectorService;

    @GetMapping(produces = "application/json")
    public ResponseEntity<List<?>> getSectors(
            @RequestParam(defaultValue = "flat", required = false) String mode
    ) {
        return ResponseEntity.ok(sectorService.getSectors(mode));
    }

    @GetMapping(path = "/id_{id}", produces = "application/json")
    public ResponseEntity<?> getSectorById(
            @RequestParam(defaultValue = "flat", required = false) String mode,
            @PathVariable long id
    ) {
        return sectorService.getSector(mode, id).map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }


    @GetMapping(path = "/{name}", produces = "application/json")
    public ResponseEntity<?> getSectorByName(
            @RequestParam(defaultValue = "flat", required = false) String mode,
            @PathVariable String name
    ) {
        return sectorService.getSector(mode, name).map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping(produces = "application/json")
    public ResponseEntity<?> postSector(@RequestBody @Valid NewSectorDto dto) {
        sectorService.addSector(dto);
        return ResponseEntity.ok().build();
    }

    @PostMapping(path = "init", produces = "application/json")
    public ResponseEntity<?> postSectorInitialData() {
        sectorService.initBasicSectors();
        return ResponseEntity.ok().build();
    }
}
package ru.deathcry.helmes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import ru.deathcry.helmes.dto.UserDto;
import ru.deathcry.helmes.entity.User;
import ru.deathcry.helmes.service.UserService;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private HttpSession session;

    @GetMapping(produces = "application/json")
    public ResponseEntity<?> getUsers(
            @RequestParam(required = false, defaultValue = "false") boolean latest
    ) {
        if(latest) return loadLatestUser();
        return ResponseEntity.ok(userService.getUsers());
    }

    @GetMapping(path = "/id_{id}", produces = "application/json")
    public ResponseEntity<?> getUserById(
            @PathVariable long id
    ) {
        return userService.getUser(id).map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }


    @GetMapping(path = "/{name}", produces = "application/json")
    public ResponseEntity<?> getUserByName(
            @PathVariable String name
    ) {
        return userService.getUser(name).map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping(produces = "application/json")
    public ResponseEntity<?> saveUser(
            @RequestBody @Valid UserDto dto
    ) {
        if (userService.userExist(dto.getName())){
            if (!dto.getName().equals(session.getAttribute("latestCreated"))) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You can't edit this user!");
            }

            User saved = userService.saveUser(dto);
            return ResponseEntity.ok("User " + saved.getName() + " was saved successfully");
        }

        User created = userService.addUser(dto);
        session.setAttribute("latestCreated", created.getName());

        return ResponseEntity.ok("User " + created.getName() + " was created successfully");
    }

    public ResponseEntity<?> loadLatestUser() {
        if(session.getAttribute("latestCreated") == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You have not created any user yet");
        }

        Optional<User> latestUser = userService.getUser(session.getAttribute("latestCreated").toString());
        if(latestUser.isEmpty()){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Something went really wrong...");
        }

        return ResponseEntity.ok(latestUser.get());
    }

    @GetMapping(path = "/{userName}/sectors",produces = "application/json")
    public ResponseEntity<?> getUserSectors(@PathVariable String userName) {
        Optional<User> x = userService.getUser(userName);
        if(x.isEmpty()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No user with such name");
        }
        return ResponseEntity.ok(x.get().getSectorIds());
    }

}
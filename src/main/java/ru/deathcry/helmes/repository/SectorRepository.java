package ru.deathcry.helmes.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.deathcry.helmes.entity.Sector;

import java.util.List;
import java.util.Optional;

@Repository
public interface SectorRepository extends CrudRepository<Sector, Long> {

    List<Sector> findAll();

    Optional<Sector> findById(Long id);

    Optional<Sector> findByName(String name);

    @Query("SELECT s FROM Sector s WHERE s.parent.id is null")
    List<Sector> findAllRootSectors();

}
